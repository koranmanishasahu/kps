package com.godigit.pack1;

import java.util.Scanner;  
class Customers {  
    private String accountNo; 
    private String accountType;
    private String customerName;  
    private long balance;  
    Scanner sc = new Scanner(System.in);  
    public void openAccount() {  
        System.out.print("Enter Account Number= ");  
        accountNo = sc.next();  
        System.out.print("Enter Account type= ");  
        accountType = sc.next();  
        System.out.print("Enter Name= ");  
        customerName = sc.next();  
        System.out.print("Enter Balance= ");  
        balance = sc.nextLong();  
    }  
   
    public boolean search(String acc_no) {  
        if (accountNo.equals(acc_no)) {  
            accountDetails();  
            return (true);  
        }  
        return (false);  
    }  
    public void deposit() {  
        long amt;  
        System.out.println("Enter the amount you want to deposit= ");  
        amt = sc.nextLong();  
        balance = balance + amt;  
    }  
     public void withdrawal() {  
        long amt;  
        System.out.println("Enter the amount you want to withdraw= ");  
        amt = sc.nextLong();  
        if (balance >= amt) {  
            balance = balance - amt;  
            System.out.println("Balance after withdrawal= " + balance);  
        } else {  
            System.out.println("Sorry! Your balance is less than " + amt);  
        }  
    }  
     public void accountDetails() {  
         System.out.println("Name of customer= " + customerName);  
         System.out.println("Account Number= " + accountNo);  
         System.out.println("Account type= " + accountType);  
         System.out.println("Balance= " + balance);  
     }  
   
}  
public class ABCBank {  
    public static void main(String arg[]) {  
        Scanner sc = new Scanner(System.in);  
        //create initial accounts  
        System.out.print("How many customers you want to input? ");  
        int n = sc.nextInt();  
        Customers C[] = new Customers[n];  
        for (int i = 0; i < C.length; i++) {  
            C[i] = new Customers();  
            C[i].openAccount();  
        }  
        int ch=0;  
        while (ch != 5) {  
            System.out.println("1. Display all account details \n 2. Search by Account number\n 3. Deposit the amount \n 4. Withdraw the amount \n 5.Exit ");  
            System.out.println("Enter your choice= ");  
            ch = sc.nextInt();  
                switch (ch) {  
                    case 1:  
                        for (int i = 0; i < C.length; i++) {  
                            C[i].accountDetails();  
                        }  
                        break;  
                    case 2:  
                        System.out.print("Enter account Number you want to search= ");  
                        String ac_no = sc.next();  
                        boolean f = false;  
                        for (int i = 0; i < C.length; i++) {  
                            f = C[i].search(ac_no);  
                            if (f) {  
                                break;  
                            }  
                        }  
                        if (!f) {  
                            System.out.println("Account doesn't exist");  
                        }  
                        break;  
                    case 3:  
                        System.out.print("Enter Account Number=");  
                        ac_no = sc.next();  
                        f = false;  
                        for (int i = 0; i < C.length; i++) {  
                            f = C[i].search(ac_no);  
                            if (f) {  
                                C[i].deposit();  
                                break;  
                            }  
                        }  
                        if (!f) {  
                            System.out.println("Account doesn't exist");  
                        }  
                        break;  
                    case 4:  
                        System.out.print("Enter Account Number= ");  
                        ac_no = sc.next();  
                        f = false;  
                        for (int i = 0; i < C.length; i++) {  
                            f = C[i].search(ac_no);  
                            if (f) {  
                                C[i].withdrawal();  
                                break;  
                            }  
                        }  
                        if (!f) {  
                            System.out.println("Account doesn't exist");  
                        }  
                        break;  
                    case 5:  
                        System.out.println("Thank you!!");  
                        break;  
                }  
            }  
             
        }  
    }  